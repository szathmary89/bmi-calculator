import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { faMars, faVenus } from '@fortawesome/free-solid-svg-icons';

import { DeldialogComponent } from '../deldialog/deldialog.component'

@Component({
  selector: 'app-bmi-calculator',
  templateUrl: './bmi-calculator.component.html',
  styleUrls: ['./bmi-calculator.component.css', './spinner.css']
})
export class BmiCalculatorComponent implements OnInit {
  @ViewChild('stepper') stepper;

  fa_mars = faMars;
  fa_venus = faVenus;

  pdFormGroup: FormGroup;
  hwFormGroup: FormGroup;

  showQuestionForm = true;
  showSpinner = false;
  showBmiResult = false;

  genderColor = "";
  bmiValue: number;
  classificationText: string;

  model = {
    nameVar: undefined,
    ageVar: 0,
    genderVar: undefined,
    heightVar: undefined,
    weightVar: undefined,
  }

  constructor(private _formBuilder: FormBuilder, public dialog: MatDialog) {}

  ngOnInit() {
    this.pdFormGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required],
      ageCtrl: ['', Validators.required],
      genderCtrl: ['', Validators.required]
    });
    this.hwFormGroup = this._formBuilder.group({
      heightCtrl: ['', Validators.required],
      weightCtrl: ['', Validators.required]
    });
  }

  ageFormatter(ageValue: number) {
    return ageValue + 'év';
  }

  clearForm() {
    this.openDialog();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DeldialogComponent, {
      width: '250px',
      data: {name: this.model.nameVar}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result === true) {
        this.stepper.reset();
      }
    });
  }

  showBmi() {
    this.showQuestionForm = false;
    this.showSpinner = true;

    setTimeout(() => {
      this.calcBmi();
      this.showSpinner = false;
      this.showBmiResult = true;
    }, 5000);
  }

  calcBmi() {
    if (this.model.genderVar === "male") {
      this.genderColor = "blue";
    }
    else if (this.model.genderVar === "female") {
      this.genderColor = "pink";
    }
    let heightMeter = this.model.heightVar/100;
    this.bmiValue = this.model.weightVar/(heightMeter**2);

    if(this.bmiValue < 16) {
      this.classificationText = "súlyos soványság";
    }
    else if(this.bmiValue < 17) {
      this.classificationText = "mérsékelt soványság";
    }
    else if(this.bmiValue < 18.5) {
      this.classificationText = "enyhe soványság";
    }
    else if(this.bmiValue < 25) {
      this.classificationText = "normális testsúly";
    }
    else if(this.bmiValue < 30) {
      this.classificationText = "túlsúlyos";
    }
    else if(this.bmiValue < 35) {
      this.classificationText = "I. fokú elhízás";
    }
    else if(this.bmiValue < 40) {
      this.classificationText = "II. fokú elhízás";
    }
    else {
      this.classificationText = "III. fokú (súlyos) elhízás";
    }
  }

  modifyFormContent() {
    this.moveToForm(0);
    this.showQuestionForm = true;
    this.showBmiResult = false;
  }

  startFromScratch() {
    this.showQuestionForm = true;
    this.showBmiResult = false;
    this.stepper.reset();
  }

  moveToForm(index: number) {
    this.stepper.selectedIndex = index;
  }

  genderToText() {
    return this.model.genderVar == 'male' ? 'Férfi' : 'Nő';
  }
}
